FROM maven:3.6.3-jdk-11-slim as java-app
WORKDIR /app
COPY app .
RUN ls -la
RUN mvn clean package

FROM tomcat:8.0
RUN echo "update v5" > file
RUN cat file 
COPY --from=java-app /app/target/Spring3HibernateApp.war /usr/local/tomcat/webapps/



